/*
========================================================================================
  StructuralVariants Nextflow config file
========================================================================================
  Default config options for all compute environments
----------------------------------------------------------------------------------------
*/

// Global default params, used in configs
params {
    //Files
    fastq = [ "data/fastq/SRR709972_{1,2}.fastq.gz",
              "data/fastq/SRR070528_{1,2}.fastq.gz",
              "data/fastq/SRR070819_{1,2}.fastq.gz",
              "data/fastq/SRR764718_{1,2}.fastq.gz",
              "data/fastq/SRR764689_{1,2}.fastq.gz"
            ]

    generate_bwa_indexes = false
    reference_fasta = "data/hs37d5.fa"
    reference_fasta_fai = "data/hs37d5.fa.fai"
    reference_fasta_indexs = [ "data/hs37d5.fa.{amb,ann,bwt,pac,sa}" ]
    samples = "data/samples_test.txt"
    bed = "data/gencode.v19.genes.v7_model.patched_contigs.gtf.bed.gz"
    bed_tbi = "data/gencode.v19.genes.v7_model.patched_contigs.gtf.bed.gz.tbi"
    blacklist = "data/hg19-blacklist.v2.num.bed.gz"

    //params
    threads_fastqc = 2
    threads_fastp = 12
    threads_bwa_mem = 24
    threads_samtools = 8
    threads_gridss = 8
    cut_right = true
    cut_right_window_size = 5
    cut_right_mean_quality = 24
    trim_tail1 = 1
    length_required = 70
    min_mapping_quality = 10
    bits_set = 4

    //cnv params
    enable_manta = true
    enable_gridss = true
    enable_exomeDepth = true
    enable_codex = true
    
    manta_exome = true
    manta_min_len = 50
    manta_max_len = 1500000
    manta_min_q = 20

    gridss_min_len = 100
    gridss_max_len = 1500000
    gridss_min_q = 999

    exomeDepth_min_len = 0.1
    exomeDepth_max_len = 200
    exomeDepth_min_bf = 5.5

    codex_min_len = 0.1
    codex_max_len = 200
    codex_min_lratio = 40

    outputDir = 'results'
    tracedir = "${params.outputDir}/pipeline_info"
}

profiles {
	debug { process.beforeScript = 'echo $HOSTNAME' }
	docker {
		docker.enabled = true
		docker.fixOwnership = true
	}
	singularity {
		singularity.autoMounts = true
		singularity.enabled = true
	}
}

manifest {
	name = 'CNV_pipeline'
  author = 'Laura Rodríguez-Navas, Adrián Muñoz-Civico, Daniel López-López'
	homePage = 'https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/tree/master/structuralvariants/nextflow'
	mainScript = 'main.nf'
	nextflowVersion = '>=19.10.0'
	version = '1.1.3'
}

// Capture exit codes from upstream processes when piping
process.shell = ['/bin/bash', '-euo', 'pipefail']

dag {
  enabled = true
  file = "${params.tracedir}/pipeline_dag.svg"
}
report {
  enabled = true
  file = "${params.tracedir}/execution_report.html"
}
timeline {
  enabled = true
  file = "${params.tracedir}/execution_timeline.html"
}
trace {
  enabled = true
  file = "${params.tracedir}/execution_trace.txt"
}
