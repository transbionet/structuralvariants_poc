# StructuralVariants Workflow

The pipeline is built with [Nextflow](https://www.nextflow.io) and [CWL](https://www.commonwl.org). It employs 
Docker/Singularity containers to make installation simple and highly reproducible. This pipeline's implementation uses 
a single container per process, making it much easier to maintain and update software dependencies.

## Requirements

See [INSTALL.md](https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/blob/master/INSTALL.md) document to install the 
necessary dependencies for this pipeline.

## Installation

```bash
wget https://gitlab.bsc.es/lrodrig1/structuralvariants_poc.git
```

Substitute `wget` with `curl` or similar.

## Usage

- Nextflow
```bash
cd structuralvariants/nextflow/
nextflow run main.nf -profile <docker/singularity>
```

Change [nextflow.config](https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/blob/master/structuralvariants/nextflow/nextflow.config) describing your inputs and additional parameters.

- CWL

```bash
cd structuralvariants/cwl/
cwltool --outdir={outputs folder} --tmpdir-prefix={intermediate folder} --tmp-outdir-prefix={intermediate folder} workflow.cwl workflow.yaml
```

Change [workflow.yaml](https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/blob/master/structuralvariants/cwl/workflow.yaml)
describing your inputs and additional parameters.

### Inputs

- FASTQ files. For example, see [phase3_WES_fastq.txt](https://gitlab.bsc.es/transbionet/wg1_standards_benchmarking/structuralvariants/-/blob/master/1stChallenge/phase3_WES_fastq.txt) that contains URLs to FASTQ files for direct download (link, SRRaccession, Sample ID, …). Be aware that several FASTQ files can be associated with one unique sample.

- Reference genome [hs37d5.fa.gz](ftp://ftp.1000genomes.ebi.ac.uk/vol1/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz).

- Exome capture design BED file and index (hg19). See examples:

    - [gencode.v19.genes.v7_model.patched_contigs.gtf.bed.gz](https://b2drop.bsc.es/index.php/s/G8dDC3pCcMMF6mp)
    - [gencode.v19.genes.v7_model.patched_contigs.gtf.bed.gz.tbi](https://b2drop.bsc.es/index.php/s/6LtGcmJHBdYxWdY)

- (Optional) ENCODE DAC blacklist BED file containing regions to ignore (hg19). Can be downloaded at: https://github.com/Boyle-Lab/Blacklist/blob/master/lists/hg19-blacklist.v2.bed.gz. 

- A descriptive file that describes the `case_id` and `batch` for each `sample_id`. See an example below:

| case_id | sample_id | batch |
|---------|-----------|-------|
| NA06985 | SRR709972 | 1     |
| NA06994 | SRR070528 | 1     |
| NA06994 | SRR070819 | 2     |
| NA07056 | SRR764718 | 1     |
| NA07357 | SRR764689 | 1     |

You can download an example file [here](https://b2drop.bsc.es/index.php/s/5J3RYZ77ACa27p6).

### Subworkflows

The workflow is divided into [subworkflows](https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/tree/master/structuralvariants/nextflow/subworkflows), which each contain the following steps:

| Step                            | Program + version          | Container                                                                          |
|---------------------------------|----------------------------|------------------------------------------------------------------------------------|
| Quality Control (raw data)      | FastQC v0.11.8             | [quay.io/biocontainers/fastqc:0.11.8--1](https://quay.io/repository/biocontainers/fastqc?tab=tags&tag=0.11.8--1)|
| Raw Data trimming               | Fastp v0.20.0              | [quay.io/biocontainers/fastp:0.20.0--hdbcaa40_0](https://quay.io/repository/biocontainers/fastp?tab=tags&tag=0.20.0--hdbcaa40_0)|
| Quality Control (pre-processed) | FastQC v.0.11.8            | quay.io/biocontainers/fastqc:0.11.8--1|
| Alignment to reference          | Bwa v.7.17 Samtools v.1.5  | [quay.io/biocontainers/bwa:0.7.17--h84994c4_5](https://quay.io/repository/biocontainers/bwa?tab=tags&tag=0.7.17--h84994c4_5)|
| Mark duplicates                 | Picard v2.10.6             | [quay.io/biocontainers/picard:2.10.6--py35_0](https://quay.io/repository/biocontainers/picard?tab=tags&tag=2.10.6--py35_0)|
| BAM filtering                   | Samtools v.1.5             | [quay.io/biocontainers/samtools:1.5--2](https://quay.io/repository/biocontainers/samtools?tab=tags&tag=1.5--2)|
| CNV calling                     | Manta v1.6.0               | [quay.io/biocontainers/manta:1.6.0--py27_0](https://quay.io/repository/biocontainers/manta?tab=tags&tag=1.6.0--py27_0)|
| CNV calling                     | GRIDSS v2.9.3              | [quay.io/biocontainers/gridss:2.9.3--0](https://quay.io/repository/biocontainers/gridss?tab=tags&tag=2.9.3--0)|
| CNV calling                     | ExomeDepth v1.1.12         | [quay.io/biocontainers/r-exomedepth:1.1.12--r36h6786f55_0](https://quay.io/repository/biocontainers/r-exomedepth?tab=tags&tag=1.1.12--r36h6786f55_0)|
| CNV calling                     | CODEX v2.9.3               | [quay.io/biocontainers/gridss:2.9.3--0](https://quay.io/repository/biocontainers/gridss?tab=tags&tag=2.9.3--0)|
| Final filtering                 | Bedtools v2.26.0           | [quay.io/biocontainers/bedtools:2.26.0gx--he513fc3_4](https://quay.io/repository/biocontainers/bedtools?tab=tags&tag=2.26.0gx--he513fc3_4)|

### Outputs

TBD


----------

![WF Visualized](https://gitlab.bsc.es/lrodrig1/structuralvariants_poc/-/raw/master/structuralvariants/docs/CNV_pipeline-cwlviewer.svg)

